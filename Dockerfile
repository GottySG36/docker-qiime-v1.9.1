FROM ubuntu:18.04

LABEL version='Beta-0.3'

RUN apt update && \
    apt upgrade -y && \
    apt install -y git \
        gnupg2 \
        python2.7 \
        software-properties-common \
        vim \
        wget

RUN apt-add-repository -y ppa:neufeldlab/ppa && \
    apt update
    
RUN ln -sf /usr/bin/python3.8 /usr/bin/python3

RUN apt install -y build-essential \
    g++ \
    gcc \
    python-dev \
    python-pip \
    python3.8 \
    python3-dev \
    python3.8-dev \
    python3-pip 

RUN pip install numpy pandas pathlib
RUN pip install biom-format==2.1.7 chart_studio cython h5py openpyxl plotly pyyaml xlrd 
RUN pip install qiime 

RUN python3 -m pip install chart-studio multiqc numpy openpyxl pandas pathlib plotly pyyaml snakemake==5.18.0 xlrd

## QIIME deps
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && apt upgrade -y

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata

RUN apt update && \
    apt install -y ant \
    build-essential \
    clang \
    cmake \
    fasttree \
    gfortran \
    ghc \
    git \
    libatlas-base-dev \
    libbz2-dev \
    libc6-i386 \
    libfreetype6-dev \
    libgsl0-dev \
    libhdf5-serial-dev \
    liblapack-dev \
    libmpich-dev \
    libmysqlclient-dev \
    libmysqlclient20 \
    libncurses5-dev \
    libperl-dev \
    libpng-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libtbb-dev \
    libxml2 \
    libxslt1-dev \
    libxslt1.1 \
    libzmq3-dev \
    mpich \
    openjdk-8-jdk \
    pandaseq \
    python-dev \
    python-tk \
    r-base \
    r-base-dev \
    rdp-classifier \
    sqlite3 \
    subversion \
    sumaclust \
    swig \
    tcl-dev \
    tk-dev \
    unzip \
    zlib1g-dev


RUN apt autoremove -y

RUN wget -q -O - ftp://ftp.ncbi.nlm.nih.gov/blast/executables/legacy.NOTSUPPORTED/2.2.22/blast-2.2.22-x64-linux.tar.gz | tar -xzf - -C /usr/bin

RUN wget -q -O - ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.10.0/ncbi-blast-2.10.0+-x64-linux.tar.gz | tar -xzf - -C /usr/local/bin 

RUN wget -q -O - http://www.bioinformatics.org/download/cd-hit/cd-hit-2007-0131.tar.gz  | tar -xzf - -C /usr/bin/ && \
    cd /usr/bin/cd-hit && \
    make -j8

RUN wget -q -O /tmp/Mothur.1.25.0.zip https://mothur.s3.us-east-2.amazonaws.com/wiki/mothur.1.25.0.zip && \
    unzip /tmp/Mothur.1.25.0.zip -d /usr/bin && \
    cd /usr/bin/Mothur.source && \
    sed -i s'/TARGET_ARCH += -arch x86_64/#TARGET_ARCH += -arch x86_64/' makefile && \
    sed -i s'/#CXXFLAGS += -mtune=native -march=native -m64/CXXFLAGS += -mtune=native -march=native -m64/' makefile && \
    make -f makefile -j8

RUN wget -q -O /tmp/ClearCut.zip https://mothur.s3.us-east-2.amazonaws.com/wiki/clearcut.source.zip && \
    unzip /tmp/ClearCut.zip -d /usr/bin && \
    cd /usr/bin/clearcut/ && \
    make -j8

RUN wget -q -O - http://sco.h-its.org/exelixis/resource/download/software/RAxML-7.3.0.tar.bz2 | tar -xjf - -C /tmp && \
    cd /tmp/RAxML-7.3.0 && \
    for f in Makefile.*; do make -j8 -f $f; rm *.o; done && \
    cp /tmp/RAxML-7.3.0/raxmlHPC*  /usr/local/bin/ && \
    rm -r /tmp/RAxML-7.3.0

RUN wget -q -O - http://eddylab.org/infernal/infernal-1.0.2.tar.gz | tar -xzf - -C /tmp && \
    cd /tmp/infernal-1.0.2/ && \
    ./configure --prefix=/usr/local && \
    make -j8 && \
    make install && \
    rm -r /tmp/infernal-1.0.2/

RUN wget -q -O - ftp://occams.dfci.harvard.edu/pub/bio/tgi/software/cdbfasta/cdbfasta.tar.gz | tar -xzf - -C /usr/bin && \
    cd /usr/bin/cdbfasta && \
    make -j8

RUN mkdir /usr/bin/muscle-3.8.31/ && \
    wget -q -O - http://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_i86linux64.tar.gz | tar -xzf - -C /usr/bin/muscle-3.8.31/ && \
    ln -s /usr/bin/muscle-3.8.31/muscle3.8.31_i86linux64 /usr/bin/muscle

RUN wget -q -O - http://static.davidsoergel.com/rtax-0.984.tgz | tar -xzf - -C /usr/bin

RUN wget -q -O - http://www.drive5.com/downloads/usearch5.2.236_i86linux32.gz | gunzip - > /usr/bin/usearch && \
    chmod +x /usr/bin/usearch

RUN wget -q -O - https://github.com/torognes/swarm/archive/v3.0.0.tar.gz | tar -zxf - -C /usr/bin && \
    cd /usr/bin/swarm-3.0.0 && \
    make -j8 && \
    make install

RUN wget -q -O /tmp/sortmerna-4.0.0-Linux.sh https://github.com/biocore/sortmerna/releases/download/v4.0.0/sortmerna-4.0.0-Linux.sh && \
    bash /tmp/sortmerna-4.0.0-Linux.sh --skip-license --prefix=/usr/local && \
    rm -r /tmp/sortmerna-4.0.0-Linux.sh

RUN git clone https://github.com/torognes/vsearch.git /tmp/vsearch && \
    cd /tmp/vsearch && \
    ./autogen.sh && \
    ./configure --prefix=/usr/local && \
    make -j8 && \
    make install && \
    rm -r /tmp/vsearch

RUN git clone https://github.com/DerrickWood/kraken2.git -b v2.0.8-beta /tmp/kraken2 && \
    cd /tmp/kraken2 && \
    ./install_kraken2.sh /usr/local/bin/kraken2 &&\
    rm -r /tmp/kraken2

RUN git clone https://github.com/DerrickWood/kraken.git /tmp/kraken && \
    cd /tmp/kraken && \
    ./install_kraken.sh /usr/local/bin/kraken && \
    rm -r /tmp/kraken

RUN wget -q -O /tmp/trimmomatic.zip http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.39.zip && \
    cd /tmp && \
    unzip trimmomatic.zip && \ 
    cd Trimmomatic-0.39 && \
    mv trimmomatic-0.39.jar /usr/local/bin && \
    echo '#!/bin/bash' > /usr/local/bin/trimmomatic && \
    echo 'java -jar /usr/local/bin/trimmomatic-0.39.jar $@' >> /usr/local/bin/trimmomatic && \
    chmod +x /usr/local/bin/trimmomatic && \
    rm -r /tmp/Trimmomatic-0.39

RUN wget -q -O /tmp/fastqc.zip https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip && \
    unzip /tmp/fastqc.zip -d /usr/local/bin && \
    chmod +x /usr/local/bin/FastQC/fastqc

RUN wget -q -O- https://mafft.cbrc.jp/alignment/software/mafft-7.471-with-extensions-src.tgz | tar -xzf - -C /tmp && \
    cd /tmp/mafft-7.471-with-extensions/core && \
    make clean && \
    make -j8 && \
    make install && \
    cd /tmp/mafft-7.471-with-extensions/extensions/ && \
    make clean && \
    make -j8 && \
    make install && \
    rm -r /tmp/mafft-7.471-with-extensions/

RUN git clone https://github.com/pachterlab/kallisto.git /tmp/kallisto && \
    cd /tmp/kallisto && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install && \
    rm -r /tmp/kallisto

RUN git clone https://salsa.debian.org/med-team/microbiomeutil.git /tmp/microbiomeutil && \
    cp -r /tmp/microbiomeutil/ChimeraSlayer /usr/bin/ChimeraSlayer && \
    rm -r /tmp/microbiomeutil

RUN mkdir /tmp/qiime & \
    wget -q -O- https://anaconda.org/bioconda/qiime/1.9.1/download/linux-64/qiime-1.9.1-np112py27_1.tar.bz2 | tar -xjf - -C /tmp/qiime && \
    cp /tmp/qiime/lib/python2.7/site-packages/qiime-1.9.1-py2.7.egg-info/scripts/uclust /usr/local/bin && \
    rm -rf /tmp/qiime

RUN mkdir /tmp/graphviz && \
    wget -q -O- https://www2.graphviz.org/Packages/stable/portable_source/graphviz-2.42.1.tar.gz | tar -C /tmp/graphviz -zxf - && \
    cd /tmp/graphviz/graphviz-2.42.1 && \
    ./autogen.sh && \
    ./configure --prefix=/usr/local && \
    make -j8 && \
    make install && \
    rm -r /tmp/graphviz

RUN mkdir /tmp/bowtie2 && \
    wget -q -O- https://github.com/BenLangmead/bowtie2/archive/v2.4.2.tar.gz | tar -C /tmp/bowtie2 -xzf - && \
    cd /tmp/bowtie2/bowtie2-2.4.2 && \
    mkdir build && \
    cd build &&\
    cmake .. && \
    make -j8 install && \
    rm -r /tmp/bowtie2

RUN mkdir -p /home/analysis

ENV PATH="/usr/local/bin/FastQC:/usr/bin/cd-hit:/usr/bin/Mothur.source:/usr/bin/rtax-0.984:/usr/bin/clearcut:/usr/bin/cdbfasta:/usr/bin/blast-2.2.22/bin:/usr/bin/ChimeraSlayer:/usr/local/bin/kraken:/usr/local/bin/kraken2:${PATH}"

RUN apt install -y locales

ENV LC_ALL="C.UTF-8"
ENV LC_CTYPE="C.UTF-8"
RUN dpkg-reconfigure locales

WORKDIR /home/analysis
